import React from 'react';
import { useRouter } from 'next/router';

const Product = () => {
	const router = useRouter();
	console.log(router);

	return <div>{router.query.id}</div>;
};

export default Product;
