const express = require('express');
const next = require('next');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

/* ------------------------------ File Imports ------------------------------ */
const authRoutes = require('./routes/authRoutes.js');
const { errorHandler } = require('./middleware/errorHandler');

dotenv.config();

const PORT = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dir: 'client', dev });
const handle = app.getRequestHandler();

app
	.prepare()
	.then(() => {
		const server = express();

		server.use(express.json());

		server.use('/api', authRoutes);

		server.use(errorHandler);

		server.get('*', (req, res) => {
			return handle(req, res);
		});

		mongoose
			.connect(process.env.MONGO_URI, {
				useNewUrlParser: true,
				useUnifiedTopology: true,
			})
			.then((result) => {
				console.log(`Connected to port ${PORT}`);
				server.listen(process.env.PORT || PORT);
			})
			.catch((err) => {
				console.log(err);
			});
	})
	.catch((ex) => {
		console.error(ex.stack);
		process.exit(1);
	});
