const bcrypt = require('bcryptjs');
// const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const asyncHandler = require('express-async-handler');
const { validationResult } = require('express-validator/check');

const userModel = require('../models/userModel');
const authModel = require('../models/authModel');
const statusCodes = require('../utils/httpStatusCodes');

exports.postRegister = asyncHandler(async (req, res) => {
	const errors = validationResult(req);

	if (!errors.isEmpty()) {
		res.status(statusCodes.UNPROCESSSABLE);
		return res.json(errors.array());
	}

	var firstName = req.body.firstName;
	var lastName = req.body.lastName;
	var handle = req.body.handle;
	var email = req.body.email;
	var password = req.body.password;

	const userExists = await userModel.findOne({
		$or: [{ email: email }, { handle: handle }],
	});

	if (userExists) {
		res.status(statusCodes.UNPROCESSSABLE);
		throw new Error('User already registered');
	}

	const hashedPassword = await bcrypt.hash(password, +process.env.SALTS);
	const newAuth = new userModel({
		handle,
		email,
		password: hashedPassword,
		firstName,
		lastName,
	});

	await newAuth.save();

	res.status(statusCodes.RESOURCE_CREATED).json({ message: 'User registered' });
});
