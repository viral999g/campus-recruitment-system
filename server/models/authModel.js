const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
	handle: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	regToken: String,
	regTokenExpiration: Date,
	resetToken: String,
	resetTokenExpiration: Date,
	verified: {
		type: Boolean,
		default: false,
		required: true,
	},
});

module.exports = mongoose.model('Auth', userSchema);
