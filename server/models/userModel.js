const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
	handle: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	verified: {
		type: Boolean,
		default: false,
		required: true,
	},
	firstName: {
		type: String,
		required: true,
	},
	lastName: {
		type: String,
		required: true,
	},
	submissions: {
		items: [
			{
				type: Schema.Types.ObjectId,
				ref: 'Submission',
			},
		],
	},
	city: String,
	country: String,
	friends: String,
	ratings: String,
	maxRatings: String,
	rank: String,
	maxRank: String,
	profileImg: String,
	avatar: String,
	subCounts: Number,
	regToken: String,
	regTokenExpiration: Date,
	resetToken: String,
	resetTokenExpiration: Date,
});

module.exports = mongoose.model('User', userSchema);
