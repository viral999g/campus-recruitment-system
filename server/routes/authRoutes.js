const express = require('express');

const authController = require('../controllers/authController');
const { validate } = require('../utils/validation');

const router = express.Router();

router
	.route('/register')
	.post(validate('registerUser'), authController.postRegister);

module.exports = router;
