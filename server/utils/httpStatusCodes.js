const httpStatusCodes = {
	OK: 200,
	RESOURCE_CREATED: 201,
	BAD_REQUEST: 400,
	UNAUTHORISED: 401,
	UNPROCESSSABLE: 422,
	INTERNAL_SERVER: 500,
};

module.exports = httpStatusCodes;
