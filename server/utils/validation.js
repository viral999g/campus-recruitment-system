const { body } = require('express-validator/check');

exports.validate = (method) => {
	switch (method) {
		case 'registerUser': {
			return [
				body('handle', 'Invalid codeforces handle').exists().bail().notEmpty(),
				body('email', 'Invalid email').exists().bail().notEmpty().isEmail(),
				body('firstName', 'Invalid first name')
					.exists()
					.bail()
					.notEmpty()
					.bail()
					.isAlpha(),
				body('lastName', 'Invalid last name')
					.exists()
					.bail()
					.notEmpty()
					.bail()
					.isAlpha(),
				body('password', 'Invalid password')
					.exists()
					.bail()
					.notEmpty()
					.bail()
					.isLength({ min: 6 })
					.withMessage('Password too short! Should be atleast 6 characters')
					.bail()
					.custom((password, { req }) => {
						if (password !== req.body.confirmPassword) {
							throw new Error('Passwords do not match');
						}

						return true;
					}),
			];
		}
	}
};
